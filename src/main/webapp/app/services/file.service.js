(function() {
    'use strict';
    angular
        .module('testApp123NewApp')
        .factory('FileService', FileService);

    FileService.$inject = ['$http'];

    function FileService ($http) {
        return {
            saveFile:function(fileDetails, File) {
                var formData = new FormData();
                formData.append('file', File);
                return $http({
                    method: 'POST',
                    url: 'http://localhost:8081/api/files',
                    data: formData,
                    params: {'filedetails': fileDetails},
                    headers: {
                        'Content-Type': undefined,
                        'Authorization': 'Bearer yJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTUzMzg5Njc4M30.FeuUu-TwFyalKhrFfs34lMHTEmbW2lROd5R9mDepM5iJw9qAZs1_I03mC8fJjbVn27donpRbYuyNKNhtMb_9rQ'
                    }
                }).then(function (response) {
                        console.log(response);
                    },
                    function (error) { // optional
                        console.log(error.status);
                    });
            }
         }
    }
})();
