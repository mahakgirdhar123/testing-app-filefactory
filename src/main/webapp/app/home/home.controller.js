(function() {
    'use strict';

    angular
        .module('testApp123NewApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService', '$state','FileService'];

    function HomeController ($scope, Principal, LoginService, $state,FileService) {
        var vm = this;
        vm.save = save;
        vm.file = {};
        function save() {
            vm.isSaving = true;
            FileService.saveFile(vm.file,document.getElementById('file').files[0]);
        }
    }
})();
