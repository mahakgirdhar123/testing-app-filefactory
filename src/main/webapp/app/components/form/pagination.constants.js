(function() {
    'use strict';

    angular
        .module('testApp123NewApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
